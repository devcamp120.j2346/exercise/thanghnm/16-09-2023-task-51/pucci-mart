$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  var gBASE_URL = "https://pucci-mart.onrender.com/api";
  // Tổng số bản ghi của hệ thống
  var gTotalRecords;
  // Tham số limit (Số lượng bản ghi tối đa trên 1 trang)
  var gPerpage = 8;
  // Tổng số trang. Math ceil để lấy số bản ghi tối đa có được. Ví dụ: 10 / 3 = 3.33 => Cần 4 trang hiển thị
  var gTotalPages;
  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading()

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  function onPageLoading() {
    // B1 Thu thập dữ liệu [không có]
    // B2 Kiểm tra dữ liệu [không có]
    // B3 Gọi Api xử lý hiển thị
    processApiToLoadFullListPet()
    makecall(1)
  }
  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm gọi Api để lấy data list pet từ server
  function processApiToLoadFullListPet() {
    $.ajax({
      url: gBASE_URL + "/pets?",
      type: "GET",
      async: false,
      success: function (responseObject) {
        // console.log(responseObject)
        // Xử lý hiển thị
        gTotalRecords = responseObject.rows.length
        gTotalPages = Math.ceil(gTotalRecords / gPerpage);
      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // hàm gọi Api để lấy 8 pet hiển thị
  function processApiToLoadListPet(paramPageNumber) {
    const vQueryParams = new URLSearchParams();

    vQueryParams.append('_limit', gPerpage);
    vQueryParams.append('_page', paramPageNumber);
    // console.log(vQueryParams.toString())
    $.ajax({
      url: gBASE_URL + "/pets?" + vQueryParams.toString(),
      type: "GET",
      async: false,
      success: function (responseObject) {
        // console.log(responseObject)
        // Xử lý hiển thị
        displayListPet(responseObject)

      },
      error: function (paramErr) {
        console.log(paramErr.status);
      }
    })
  }
  // Hàm xử lý hiển thị list Pet
  function displayListPet(paramResponse) {
    $("#list-pet").html("")
    for (var bI = 0; bI < paramResponse.rows.length; bI++) {
      var vHtml = `
      <div class="col-sm-12 col-md-6 col-lg-3 col-xl-3">
      <div class="card">
          <img class="list-img" src="${paramResponse.rows[bI].imageUrl}" alt="">
          <div class="discount discount-${paramResponse.rows[bI].id}">
          <img class="discount-img" src="images/Ellipse 4.png" alt="">
          <p>${paramResponse.rows[bI].discount}%</p>
      </div>
      </div>
      <h5>${paramResponse.rows[bI].name}</h5>
      <p>${paramResponse.rows[bI].description}</p>
      <p>$${paramResponse.rows[bI].promotionPrice}<span>$${paramResponse.rows[bI].price}</span></p>
  </div>
      `
      $("#list-pet").append(vHtml)
      if (paramResponse.rows[bI].discount == 0 || paramResponse.rows[bI].discount == null) {
        $(`.discount-${paramResponse.rows[bI].id}`).hide()
      }

    }
  }
  //Hàm pagination
  function createPagination(paramPageNumber) {
    // Xóa trắng phần tử cũ
    $("#list-pet").html("");

    // Nếu tran hiện tại là trang 1 thì nút Prev sẽ bị disable
    if (paramPageNumber == 1) {
      $(".btn-previous").attr({ "disabled": true, "href": "javascript:void(0)" })
    } else {

      $(".btn-previous").attr("disabled", false).on("click", function () {
        makecall(paramPageNumber - 1)
      })
    }
    // Nếu tran hiện tại là trang cuối cùng thì nút Next sẽ bị disable
    if (paramPageNumber == gTotalPages) {

      $(".btn-next").attr({ "disabled": true, "href": "javascript:void(0)" })
    } else {
      $(".btn-next").attr("disabled", false).on("click", function () {
        makecall(paramPageNumber + 1)
      })

    }
  }
  function makecall(paramPageNumber) {

    // Hàm tạo thanh phân trang
    createPagination(paramPageNumber);
    // Hàm hiển thị dữ liệu dựa vào số trang
    processApiToLoadListPet(paramPageNumber);
  }
})

