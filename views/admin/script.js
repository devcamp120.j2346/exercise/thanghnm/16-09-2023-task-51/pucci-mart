"use strict";
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    var gBASE_URL = "https://pucci-mart.onrender.com/api";
    const gPRODUCT_COLUMNS = ["Stt", "type", "name", "description", "imageUrl", "price", "promotionPrice", "discount", "action"]
    const gCOLUMN_STT = 0
    const gCOLUMN_TYPE = 1
    const gCOLUMN_NAME = 2
    const gCOLUMN_DESCRIPTION = 3
    const gCOLUMN_IMAGE_URL = 4
    const gCOLUMN_PRICE = 5
    const gCOLUMN_PROMOTION_PRICE = 6
    const gCOLUMN_DISCOUNT = 7
    const gCOLUMN_ACTION = 8
    var gStt = 0

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

    onPageLoading()
    $('.slider').bootstrapSlider()
    var gTable = $("#pet-table").DataTable({
        // async:false,
        // Phân trang
        paging: true,
        // Tìm kiếm
        searching: false,
        // Sắp xếp
        ordering: false,
        // Số bản ghi phân trang
        lengthChange: false,
        columns: [
            { data: gPRODUCT_COLUMNS[gCOLUMN_STT] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_TYPE] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_NAME] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_DESCRIPTION] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_IMAGE_URL] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_PRICE] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_PROMOTION_PRICE] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_DISCOUNT] },
            { data: gPRODUCT_COLUMNS[gCOLUMN_ACTION] }


        ],
        columnDefs: [
            {
                targets: gCOLUMN_STT,
                render: renderStt
            },
            {
                targets: gCOLUMN_ACTION,
                defaultContent: `<button class="btn btn-edit">
                                    <i class="fa-solid fa-pen-to-square"></i>
                                    </button>
                                    <button class="btn btn-delete">
                                    <i class="fa-solid fa-trash-can"></i>
                                    </button>`
            },
            {
                targets: gCOLUMN_IMAGE_URL,
                render: renderImg
            },
            {
                targets: gCOLUMN_PRICE,
                render: renderPrice
            },
            {
                targets: gCOLUMN_PROMOTION_PRICE,
                render: renderPrice
            },
            {
                targets: gCOLUMN_DISCOUNT,
                render: renderDiscount
            }
        ]
    })
    //Gán sự kiện cho nút thêm pet
    $(document).on("click", "#btn-add-pet", function () {
        $("#add-modal").modal("show")
    })
    // Gán sự kiện cho nút confirm thêm pet
    $(document).on("click", "#btn-confirm-add-pet", function () {
        onBtnAddNewPetClick()
    })
    // Gán sự kiện cho nút Edit
    $(document).on("click", ".btn-edit", function () {
        onBtnEditClick(this)
    })
    // Gán sự kiện cho nút Cập nhật
    $(document).on("click", "#btn-confirm-update", function () {
        onBtnUpdateClick()
    })
    // Gán sự kiện cho nút Delete
    $(document).on("click", ".btn-delete", function () {
        onBtnDeleteClick(this)
    })
    // Gán sự kiện cho nút xóa pet
    $(document).on("click", "#btn-confirm-delete-pet", function () {
        onBtnConfirmDeleteClick()
    })
    $('.slider').on("mouseup", function () {
        filterPetBySlider()
    })
    // 



    // 
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {

        gStt = 0
        // B1 Thu thập dữ liệu
        // B2 Kiểm tra dữ liệu
        // B3 Gọi Api
        processApiToLoadPetList()
    }
    function onBtnAddNewPetClick() {
        var vObject = {
            type: "",
            name: "",
            description: "",
            imageUrl: "",
            price: 0,
            promotionPrice: 0,
            discount: null
        }
        // B1 Thu thập dữ liệu
        getDataToAddPet(vObject)
        // B2 Kiểm tra dữ liệu
        var vValidate = isPetDataValidate(vObject)
        if (vValidate == true) {
            //B3 Gọi Api để thêm vào danh sách
            processApiToAddPet(vObject)

        }
    }
    // Hàm xử lý sự kiện nút edit click
    function onBtnEditClick(paramBtn) {
        var vRowSelected = $(paramBtn).parents("tr")
        var vDataRow = gTable.row(vRowSelected).data()
        $("#update-modal").modal("show")
        $("#select-update-type").val($.trim(vDataRow.type))
        $("#input-update-name").val($.trim(vDataRow.name))
        $("#input-update-description").val($.trim(vDataRow.description))
        $("#input-update-img-url").val($.trim(vDataRow.imageUrl))
        $("#input-update-price").val($.trim(vDataRow.price))
        $("#input-update-promotion-price").val($.trim(vDataRow.promotionPrice))
        $("#input-update-discount").val($.trim(vDataRow.discount))
        $("#btn-confirm-update").attr("petId", vDataRow.id)
    }
    // Hàm xử lý sự kiện nút edit click
    function onBtnDeleteClick(paramBtn) {
        var vRowSelected = $(paramBtn).parents("tr")
        var vDataRow = gTable.row(vRowSelected).data()
        $("#delete-confirm-modal").modal("show")
        $("#btn-confirm-delete-pet").attr("petId", vDataRow.id)
    }
    // Hàm xử lý sự kiện click nút cập nhật
    function onBtnUpdateClick() {
        var vObject = {
            type: "",
            name: "",
            description: "",
            imageUrl: "",
            price: 0,
            promotionPrice: 0,
            discount: null
        }
        // B1 thu thập dữ liệu
        getDataToUpdate(vObject)
        // B2 Kiểm tra dữ liệu
        var vValidate = isPetDataValidate(vObject)
        if (vValidate == true) {
            // B3 Gọi Api
            processApiToUpdatePet(vObject)
        }
    }
    function onBtnConfirmDeleteClick() {
        // B1 Thu thập dữ liệu
        // B2 Kiểm tra dữ liệu
        // B3 Gọi Api
        processDeletePet()
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    // Hàm render Cột số thứ tự
    function renderStt() {
        gStt++
        return gStt
    }
    //Hàm render Img
    function renderImg(paramUrl) {
        return `<img class="img-product" src="${paramUrl}"/>`
    }
    //Hàm render Price
    function renderPrice(paramPrice) {
        return `<p class="pet-price" data-price='${paramPrice}'>${paramPrice}</p>`
    }
    // hàm render Discount
    function renderDiscount(paramDiscount) {
        if (paramDiscount !== null && paramDiscount !== 0) {
            return `${paramDiscount}%`
        }
        else {
            return ""
        }
    }

    // hàm gọi Api để load Pet list
    function processApiToLoadPetList() {
        $.ajax({
            url: gBASE_URL + "/pets/",
            type: "GET",
            // async:false,
            success: function (responseObject) {
                displayDataToTable(responseObject.rows)
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        })
    }
    function displayDataToTable(paramObject) {
        gStt = 0
        gTable.clear();
        gTable.rows.add(paramObject);
        gTable.draw();
    }
    // Hàm thu thập dữ liệu để thêm mới thú cưng
    function getDataToAddPet(paramObject) {
        paramObject.type = $.trim($("#select-add-type").val())
        paramObject.name = $.trim($("#input-add-name").val())
        paramObject.description = $("#input-add-description").val()
        paramObject.imageUrl = $("#input-add-img-url").val()
        paramObject.price = $.trim($("#input-add-price").val())
        paramObject.promotionPrice = $.trim($("#input-add-promotion-price").val())
        paramObject.discount = $.trim($("#input-add-discount").val())
        console.log(paramObject)
    }
    // Hàm kiểm tra dữ liệu
    function isPetDataValidate(paramObject) {
        if (paramObject.name == "") {
            alert("Hãy nhập tên thú cưng")
            return false
        }
        if (paramObject.description == "") {
            alert("Hãy nhập mô tả")
            return false
        }
        if (paramObject.imageUrl == "") {
            alert("Hãy nhập đường dẫn hình ảnh")
            return false

        }
        if (paramObject.price == "") {
            alert("Hãy nhập giá tiền")
            return false

        }
        if (paramObject.promotionPrice == "") {
            alert("Hãy nhập giá giảm")
            return false

        }
        if (paramObject.discount == "") {
            paramObject.discount = null
        }
        return true
    }
    // Hàm gọi Api để thêm pet
    function processApiToAddPet(paramObject) {
        $.ajax({
            url: gBASE_URL + "/pets",
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObject),
            success: function (response) {
                // B4: xử lý front-end
                alert(`
        Thêm thú cưng thành công
        `)
                $("#add-modal").modal("hide")
                onPageLoading()

            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
    // Hàm thu thập dữ liệu cho nút UPDATE
    function getDataToUpdate(paramObject) {
        paramObject.type = $.trim($("#select-update-type").val())
        paramObject.name = $.trim($("#input-update-name").val())
        paramObject.description = $.trim($("#input-update-description").val())
        paramObject.imageUrl = $.trim($("#input-update-img-url").val())
        paramObject.price = $.trim($("#input-update-price").val())
        paramObject.promotionPrice = $.trim($("#input-update-promotion-price").val())
        paramObject.discount = $.trim($("#input-update-discount").val())
    }

    // Hàm gọi Api để update pet
    function processApiToUpdatePet(paramObject) {
        $.ajax({
            url: gBASE_URL + "/pets/" + $("#btn-confirm-update").attr("petId"),
            type: "PUT",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify(paramObject),
            success: function (responseObject) {
                alert(responseObject.message)
                $("#update-modal").modal("hide")
                onPageLoading()

            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        })
    }
    // Hàm gọi Api xóa thú cưng
    function processDeletePet() {
        $.ajax({
            url: gBASE_URL + "/pets/" + $("#btn-confirm-delete-pet").attr("petId"),
            type: "DELETE",
            success: function (response) {
                alert(response.message);
                $("#delete-confirm-modal").modal("hide")
                onPageLoading()
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        });
    }
    // Hàm filter
    function filterPetBySlider() {
        var vMin = $(".min-slider-handle").attr("aria-valuenow")
        var vMax = $(".max-slider-handle").attr("aria-valuenow")
        const vQueryParams = new URLSearchParams();
        vQueryParams.append('priceMin', vMin);
        vQueryParams.append('priceMax', vMax);
        $.ajax({
            url: gBASE_URL + "/pets?" + vQueryParams.toString(),
            type: "GET",
            // async:false,
            success: function (responseObject) {
                displayDataToTable(responseObject.rows)
            },
            error: function (paramErr) {
                console.log(paramErr.status);
            }
        })
    }



})

